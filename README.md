# mobile2city中国手机号转换城市
* CityInfo:332行
* CityMobile:216064行

###mssql
```sql
CREATE TABLE [CityInfo] (
    [CityCode] varchar(4) NOT NULL,
    [CityName] nvarchar(50) Default (N'''') NOT NULL,
    [ProvinceCode] int Default 0 NOT NULL,
    [Province] nvarchar(50) Default (N'''') NOT NULL,
    PRIMARY KEY ([CityCode])
);
CREATE TABLE [CityMobile] (
    [Mobile] varchar(7) NOT NULL,
    [CityCode] varchar(4) Default (N'''') NOT NULL,
    [CityName] nvarchar(50) Default (N'''') NOT NULL,
    [ProvinceCode] int Default 0 NOT NULL,
    [Province] nvarchar(50) Default (N'''') NOT NULL,
    [CstCode] int Default 0 NOT NULL,
    [PostCode] varchar(6) Default (N'''') NOT NULL,
    PRIMARY KEY ([Mobile])
);
```
###mysql
```sql
CREATE TABLE `CityInfo` (
    `CityCode` varchar(4) NOT NULL COMMENT '区号编号',
    `CityName` varchar(50) Default "\'" NOT NULL COMMENT '区名称',
    `ProvinceCode` int Default 0 NOT NULL COMMENT '省份编号',
    `Province` varchar(50) Default "\'" NOT NULL COMMENT '省份',
    PRIMARY KEY (`CityCode`)
);
CREATE TABLE `CityMobile` (
    `Mobile` varchar(7) NOT NULL COMMENT '手机号前7位',
    `CityCode` varchar(4) Default "\'" NOT NULL COMMENT '区号',
    `CityName` varchar(50) Default "\'" NOT NULL COMMENT '区名',
    `ProvinceCode` int Default 0 NOT NULL COMMENT '省份编号',
    `Province` varchar(50) Default "\'" NOT NULL COMMENT '省份',
    `CstCode` int Default 0 NOT NULL COMMENT '运营商编号',
    `PostCode` varchar(6) Default "\'" NOT NULL COMMENT '邮编',
    PRIMARY KEY (`Mobile`)
);
```
###sqlite
```sql
CREATE TABLE "CityInfo" (
    "CityCode" TEXT(4) NOT NULL,
    "CityName" TEXT(50) Default "'" NOT NULL,
    "ProvinceCode" INTEGER Default 0 NOT NULL,
    "Province" TEXT(50) Default "'" NOT NULL,
    PRIMARY KEY ("CityCode")
);
CREATE TABLE "CityMobile" (
    "Mobile" TEXT(7) NOT NULL,
    "CityCode" TEXT(4) Default "'" NOT NULL,
    "CityName" TEXT(50) Default "'" NOT NULL,
    "ProvinceCode" INTEGER Default 0 NOT NULL,
    "Province" TEXT(50) Default "'" NOT NULL,
    "CstCode" INTEGER Default 0 NOT NULL,
    "PostCode" TEXT(6) Default "'" NOT NULL,
    PRIMARY KEY ("Mobile")
);
```
###oracle
```sql
CREATE TABLE CityInfo (
    CityCode varchar2(4) NOT NULL,
    CityName nvarchar2(50) Default '''' NOT NULL,
    ProvinceCode NUMBER(10) Default 0 NOT NULL,
    Province nvarchar2(50) Default '''' NOT NULL,
    PRIMARY KEY (CityCode)
);
CREATE TABLE CityMobile (
    Mobile varchar2(7) NOT NULL,
    CityCode varchar2(4) Default '''' NOT NULL,
    CityName nvarchar2(50) Default '''' NOT NULL,
    ProvinceCode NUMBER(10) Default 0 NOT NULL,
    Province nvarchar2(50) Default '''' NOT NULL,
    CstCode NUMBER(10) Default 0 NOT NULL,
    PostCode varchar2(6) Default '''' NOT NULL,
    PRIMARY KEY (Mobile)
);
```
###access
```sql
CREATE TABLE [CityInfo] (
    [CityCode] text(4) NOT NULL,
    [CityName] text(50) Default "'",
    [ProvinceCode] int Default 0,
    [Province] text(50) Default "'",
    PRIMARY KEY ([CityCode])
);
CREATE TABLE [CityMobile] (
    [Mobile] text(7) NOT NULL,
    [CityCode] text(4) Default "'",
    [CityName] text(50) Default "'",
    [ProvinceCode] int Default 0,
    [Province] text(50) Default "'",
    [CstCode] int Default 0,
    [PostCode] text(6) Default "'",
    PRIMARY KEY ([Mobile])
);
```

###工具
```
git clone https://github.com/LiveXY/mobile2city.git
cd mobile2city/
npm install && npm link

mobile2city 13701862737 13703762738
#13701862737 : 0210 200000 上海 上海
#13703762738 : 0376 465150 河南 潢川

ip2address '125.42.204.47;125.42.204.45;61.53.186.5;125.42.204.46;222.73.202.202'
#  125.42.204.47 : 河南省 信阳市  { y: '3757267.65', x: '12700076.95' } baidu
#  125.42.204.45 : 中国 河南省 信阳市  taobao
#    61.53.186.5 : 中国 河南省 平顶山市  taobao2
#  125.42.204.46 : 中国 河南 信阳  ipip
# 222.73.202.202 : 中国 上海 上海  sina
ip2address baidu '125.42.204.47;125.42.204.45;61.53.186.5;125.42.204.46;222.73.202.202'
```

###手机号转区域
```
131,10000
135,10000
136,10000
150,10000
151,10000
152,10000
155,10000
158,10000
159,10000
132,10000
139,9998
189,9995
138,9994
187,9980
137,9976
183,9975
182,9970
181,9965
186,9960
130,9879
177,9851
153,9848
180,9837
156,9802
188,9750
134,9746
147,9719
133,9648
184,9074
185,9055
145,8378
157,8266
176,7436
178,6969

电信号段:133/153/180/181/189/177
联通号段:130/131/132/155/156/185/186/145/176
移动号段:134/135/136/137/138/139/150/151/152/157/158/159/182/183/184/187/188/147/178
```